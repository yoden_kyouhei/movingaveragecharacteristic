﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApplication1
{
	class Program
	{
		const int DataSize = 10000;
		static double orgFunction(double t)
		{
			//= 80 * SIN(A1)
			return 80 * Math.Sin(t);
		}
		static double noiseFunction(double t)
		{
			//= 80 * SIN(A1) + 18 * SIN(10 * A1)
			return 80 * Math.Sin(t) + 18 * Math.Sin(10 * t);
		}
		static List<double> idouheikin(List<double> list,int N)
		{
			List<double> Return = new List<double>();
			for(int index = N; index < list.Count; index++)
			{
				double sum = 0;
				for (int i = 1; i <= N; i++)
				{
					sum += list[index - i];
				}
				sum /= N;
				Return.Add(sum);
			}
			return Return;
		}
		static double gosaritu(List<double> org,List<double> heikin_noise,int N)
		{
			double sum = 0;
			for(int index = 0; index < heikin_noise.Count; index++)
			{
				sum += Math.Abs((heikin_noise[index] - org[index + (N/2)]) / org[index + (N/2)]);
			}
			sum /= heikin_noise.Count;
			return sum * 100;
		}
		static void Main(string[] args)
		{
			Process();
		}
		static void Process()
		{
			double[,] data = new double[128,2];
			Parallel.For(0, 128, ID =>
			  {
				  int N = (ID + 1) * 2 + 1;
				  List<double[]> gosa = new List<double[]>();
				  double[] org = new double[DataSize];
				  double[] noise = new double[DataSize];
				  for (double T = 0.0001; T <= 0.1; T += 0.0001)
				  {
					  Parallel.For(0, DataSize, id =>
					  {
						  org[id] = orgFunction(T * id);
						  noise[id] = noiseFunction(T * id);
					  });
					  List<double> heikin_noise = new List<double>(idouheikin(noise.ToList(), N));
					  double[] value = new double[2];
					  value[0] = T;
					  value[1] = gosaritu(org.ToList(), heikin_noise, N);
					  gosa.Add(value);
				  }
				  double[] minvalue = gosa[0];
				  {
					  foreach (var obj in gosa)
					  {
						  if (minvalue[1] > obj[1])
						  {
							  minvalue = obj;
						  }
					  }
				  }
				  data[ID, 0] = N;
				  data[ID, 1] = minvalue[0];
			  });
			{
				StreamWriter sw = new StreamWriter("housoku.csv");
				sw.WriteLine("N,T,");
				for(int index = 0; index < 128; index++)
				{
					sw.WriteLine(data[index,0]+","+ data[index, 1]+",");
				}
				sw.Close();
			}
		}
		static void kankaku()
		{
			int N = 31;
			List<double[]> gosa = new List<double[]>();
			double[] org = new double[DataSize];
			double[] noise = new double[DataSize];
			for (double T = 0.0001; T <= 0.1; T += 0.0001)
			{
				Console.WriteLine(T);
				Parallel.For(0, DataSize, id =>
				  {
					  org[id] = orgFunction(T * id);
					  noise[id] = noiseFunction(T * id);
				  });
				List<double> heikin_noise = new List<double>(idouheikin(noise.ToList(), N));
				double[] value = new double[2];
				value[0] = T;
				value[1] = gosaritu(org.ToList(), heikin_noise, N);
				gosa.Add(value);
			}

			{
				StreamWriter sw = new StreamWriter("kankaku.csv");
				foreach(var obj in gosa)
				{
					sw.WriteLine(obj[0] + "," + obj[1] + ",");
				}
				sw.Close();
			}
		}
		static void tensuu()
		{
			double T = 0.02;
			List<double[]> gosa = new List<double[]>();
			double[] org = new double[DataSize];
			double[] noise = new double[DataSize];
			for (int N = 3; N <= 511; N+=2)
			{
				Console.WriteLine(N);
				Parallel.For(0, DataSize, id =>
				{
					org[id] = orgFunction(T * id);
					noise[id] = noiseFunction(T * id);
				});
				List<double> heikin_noise = new List<double>(idouheikin(noise.ToList(), N));
				double[] value = new double[2];
				value[0] = N;
				value[1] = gosaritu(org.ToList(), heikin_noise, N);
				gosa.Add(value);
			}

			{
				StreamWriter sw = new StreamWriter("tennsuu.csv");
				foreach (var obj in gosa)
				{
					sw.WriteLine(obj[0] + "," + obj[1] + ",");
				}
				sw.Close();
			}
		}
	}
}
